

let arr = []


function taoArr() {
    let number = document.querySelector(".number1").value*1;
    arr.push(number)
    let resual = document.querySelector(".resual1 span");
    resual.innerHTML = arr;
}


// bài 1
function tongSoDuong(){
    let click2 = document.querySelector(".click2")
    click2.addEventListener("click",function(){
    let tong = 0;
    for(let i = 0; i< arr.length; i++){
        if(arr[i] >0){
            tong = tong + arr[i]
            document.querySelector(".resual2 span").innerHTML = `<span>Tổng số dương: ${tong}</span>`
            }
        }
    }) 
}
tongSoDuong()

// bai 2
function demSoDuong(){
    let click3 = document.querySelector(".click3")
    let count = 0;
    click3.addEventListener("click",function(){
        for(let i = 0 ; i<arr.length; i++){
            if(arr[i] >0){
                count++;
                document.querySelector(".resual3 span").innerHTML = `<span>Số dương: ${count}</span>`
            }
        }
    })
}
demSoDuong()

// Bài 3
function soNhoNhat(){
    let click4 = document.querySelector(".click4")
    click4.addEventListener("click",function(){
        let soNhoNhat = arr[0]
        for(let i = 0 ; i<arr.length; i++){
            let list = arr[i]
            if(soNhoNhat > list || soNhoNhat == list){
                soNhoNhat = list;
                document.querySelector(".resual4 span").innerHTML = `<span>Số nhỏ nhất: ${soNhoNhat}</span>`
            }
        }
    })
}
soNhoNhat()

// Bài 4
function soDuongNhoNhat(){
    let click5 = document.querySelector(".click5");
    let arrSoDuong = [];
    click5.addEventListener("click",function(){
        for(let i = 0; i<arr.length; i++){
            if(arr[i]>0){
                arrSoDuong.push(arr[i])
                let soDuongNhoNhat = arrSoDuong[0];
                for(let i = 0; i< arrSoDuong.length; i++){
                    let text = arrSoDuong[i];
                    if(soDuongNhoNhat > text || soDuongNhoNhat == text){
                        soDuongNhoNhat = text
                        console.log(soDuongNhoNhat)
                        document.querySelector(".resual5 span").innerHTML = `<span>Số dương nhỏ nhất: ${soDuongNhoNhat}</span>`
                    }
                }
            }
        }
    })
}
soDuongNhoNhat()

// Bài 5
function timSoChanCuoiCung(){
    let arrSoChan=[];
    for(let i = 0; i<arr.length; i++){
        if(arr[i]%2==0){
            let number = arr[i]
            arrSoChan.push(number)
            let length = arrSoChan.length
            let indexSoCuoi = length - 1;
            let coChanCuoi = arrSoChan[indexSoCuoi]
            document.querySelector(".resual6 span").innerHTML = `<span>Số chẵn cuối là:${coChanCuoi}</span> `
        }
    }
}

// bài6
function doiCho(){
    let index1 = document.querySelector(".index1").value*1;
    let index2 = document.querySelector(".index2").value*1;
    var x = arr[index1];
    arr[index1] = arr[index2]
    arr[index2] = x

    let resual =  document.querySelector(".resual7 span")
    resual.innerHTML = ` <span>Mảng sau khi đổi là</span> ${arr}`
}

// bài 7

function sapXepTang(){
    let min;
    for(let i = 0; i< arr.length; i++){ 
        min = i
        for (let j = i + 1; j < arr.length; j++){
            if (arr[min] > arr[j]){
                min = j;
            }
        }
        if (i !== min){
            let temp = arr[i];
            arr[i] = arr[min];
            arr[min] = temp;
        }
    }
    document.querySelector(".resual8 span").innerHTML = `${arr}`;
}


// Bài 8

// Cách 1
// function  inSoNguyenTo(){
//     for(let i=0; i< arr.length; i++){
//         let flag = true;
//         if(arr[i]<2){
//             flag = false;
//         }else{
//             for(let x = 2 ; x <= Math.sqrt(arr[i]); x++){
//                 if(arr[i] % x == 0){
//                     flag = false;
//                 }
//             }
//         }
//         if(flag == true){
//             document.querySelector(".resual9 span").innerHTML = arr[i];
//             break
//         }
//     }
// }

// cách 2
function timSoNguyenTo(n){
    if (n < 2) {
        return false;
    }
    for(let x = 2 ; x <= Math.sqrt(n); x++){
        if(n % x == 0){
            return false;
        }
    }
    return true;
}

function inSoNguyenTo(){
    let n = -1;
    for(let i = 0; i < arr.length;i++){
        if(timSoNguyenTo(arr[i])){
            n = arr[i];
            break;
        }
    }
    document.querySelector(".resual9 span").innerHTML = `Số nguyên tố đầu tiên là: ${n}`;
}


// bài 9

let arrSoThuc = [];

function pushSoThuc(){
    let soThuc = document.querySelector(".nhapSoThuc").value*1;
    arrSoThuc.push(soThuc)
    document.querySelector(".resual10 span").innerHTML= `${arrSoThuc}`
}

function demSoNguyen(){
    let n = 0;
    for(let i = 0 ; i<arrSoThuc.length ; i++){
        let check = Number.isInteger(arrSoThuc[i])
        if(check == true){
            n++;
            document.querySelector(".resual11 span").innerHTML = n
        }
        
    }
}


// Bài 10

function soSanhAnDuong(){
    let soDuong=0;
    let soAm=0;
    for(let i = 0; i<arr.length; i++){
        if(arr[i] > 0){
            soDuong++
            console.log(soDuong)
        }else{
            soAm++
            console.log(soAm)
        }

        if(soAm > soDuong){
            document.querySelector(".resual12 span").innerHTML = `<span>Số âm > Số dương </span>`
        }else if(soAm < soDuong){
            document.querySelector(".resual12 span").innerHTML = `<span>Số âm < Số dương </span>`
        }else{
            document.querySelector(".resual12 span").innerHTML = `<span>Số âm = Số dương </span>`
        }
    }
};